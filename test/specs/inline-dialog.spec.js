const globals = require('../globals');
const ProductPage = require('../pageobjects/ProductPage');

describe('Inline dialog', () => {

  beforeAll(() => {
    ProductPage.open();
  });

  it('webitem should launch an inline dialog', () => {
    browser.waitForVisibleAndClick(ProductPage.inlineDialogMenuItem);
    browser.waitUntil(
      () => ProductPage.numberOfInlineDialogIframes === 1,
      browser.getConfig().waitForTimeout,
      'No inline dialog iframe was added to the DOM'
    );
  });
});