const globals = require('../globals');
const DialogPage = require('../pageobjects/DialogPage');
const RefAppPage = require('../pageobjects/KitchenSinkRefAppPage');
const ProductPage = require('../pageobjects/ProductPage');

describe('Dialog page', () => {

  const waitForTimeout = browser.getConfig().waitForTimeout;

  beforeAll(() => {
    ProductPage.open();
    browser.waitForVisibleAndClick(ProductPage.kitchenSinkAppMenuItem);

    browser.selectFrame(globals.kitchen_sink_refapp_selector);
    browser.waitForVisibleAndClick(RefAppPage.dialogMenuItem);
  });

  describe('Create dialog button', () => {
    it('should launch a dialog', () => {
      browser.waitForVisibleAndClick(RefAppPage.createDialogButton);

      browser.selectTopWindow();
      browser.waitUntil(
        () => ProductPage.numberOfDialogIframes === 1,
        waitForTimeout,
        'Dialog iframe did not get added to the DOM'
      );
    });

    it('should launch a dialog from a dialog', () => {
      const dialog1Id = browser.selectFrame(globals.dialog_selector,0);
      browser.waitForVisibleAndClick(DialogPage.createAnotherTab);
      browser.waitForVisibleAndClick(DialogPage.createAnotherDialogButton);

      browser.selectTopWindow();
      browser.waitUntil(
        () => ProductPage.numberOfDialogIframes === 2,
        waitForTimeout,
        'Second dialog iframe did not get added to the DOM'
      );

      const dialog2Id = browser.selectFrame(globals.dialog_selector,1);
      expect(dialog1Id).not.toEqual(dialog2Id);
    });
  });
});