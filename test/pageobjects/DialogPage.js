class DialogPage {
  get createAnotherTab() {return $('li=Create another')}
  get createAnotherDialogButton() {return $('button=Create another dialog')}
}

module.exports = new DialogPage();