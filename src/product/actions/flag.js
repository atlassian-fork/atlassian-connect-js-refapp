export const CREATE_FLAG = 'createFlag';
export const CLOSE_FLAG = 'closeFlag';
export const DISMISS_FLAG = 'dismissFlag';

export const createFlag = (options) => {
  return {
    type: CREATE_FLAG,
    options
  };
};

export const closeFlag = (id) => {
  return {
    type: CLOSE_FLAG,
    id
  };
};

export const dismissFlag = (id) => {
  return {
    type: DISMISS_FLAG,
    id
  };
};
