import React, { PureComponent } from 'react';
import AkSpinner from '@atlaskit/spinner';

export default class IframeLoadingIndicator extends PureComponent {

  render() {
    const containerStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      height: 'auto',
      width: 'auto',
      zIndex: 1,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    };
    return (
      <div style={containerStyle}>
        <AkSpinner size='large'/>
      </div>
    );
  }

}
