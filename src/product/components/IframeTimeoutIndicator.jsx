import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

export default class IframeTimeoutIndicator extends PureComponent {

  handleClick(e) {
    e.preventDefault();
    this.props.failedCallback();
  }

  render() {
    const containerStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      height: 'auto',
      width: 'auto',
      zIndex: 1,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    };
    return (
      <div style={containerStyle}>
        <AkSpinner size='large' />
        <div>
          Wait or <Button appearance="link" spacing="none" onClick={this.handleClick.bind(this)}>cancel</Button>?
        </div>
        <div>
          (Check the <a href="/#/settings">settings</a> to see if iframe timeouts are enabled)
        </div>
      </div>
    );
  }

}
