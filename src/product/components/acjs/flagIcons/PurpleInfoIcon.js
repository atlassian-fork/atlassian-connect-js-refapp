import React, { PureComponent } from 'react';
import InfoIcon from '@atlaskit/icon/glyph/info';
import { akColorP300 } from '@atlaskit/util-shared-styles';

/**
 * This component renders a purple info icon.
 */
export default class PurpleInfoIcon extends PureComponent {
  render() {
    return (
      <div style={{ color: akColorP300 }}>
        <InfoIcon label="Info" />
      </div>
    );
  }
}
