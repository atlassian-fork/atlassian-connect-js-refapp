import React, { PureComponent } from 'react';
import GenericIcon from '@atlaskit/icon/glyph/info';
import { akColorN300 } from '@atlaskit/util-shared-styles';

/**
 * This component renders a grey generic icon.
 */
export default class GreyGenericIcon extends PureComponent {
  render() {
    return (
      <div style={{ color: akColorN300 }}>
        <GenericIcon label="Generic" />
      </div>
    );
  }
}
