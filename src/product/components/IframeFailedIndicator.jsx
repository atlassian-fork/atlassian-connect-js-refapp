import React, { PureComponent } from 'react';
import failedToLoad from '../assets/failedToLoad.jpg';

export default class IframeFailedIndicator extends PureComponent {

  render() {
    const message = 'Oops, the app failed to load.';
    return (
      <div>
        <p>
          {message}
        </p>
        <img src={failedToLoad} alt="Failed to load" />
        <p>
          This is the reference app so check your settings to ensure you are not intentionally timing out dialogs.'
        </p>
      </div>
    );
  }

}
