
export default class ContextParameter {

  constructor(key, value) {
    this.key = key;
    this.value = value;
  }

}
