import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router';
import { HashRouter } from 'react-router-dom';
import AppDAO from "../appmanagement/AppDAO";
import { AppLinkUtil } from '../appmanagement/AppNavLinks';
import ProductApp from './ProductApp.jsx';
import HomePage from '../pages/HomePage.jsx';
import JiraPage from '../pages/jira/JiraPage.jsx';
import SettingsPage from '../pages/SettingsPage.jsx';
import GenericAppHostPage from '../pages/GenericAppHostPage.jsx';

export default class MainRouter extends PureComponent {
  constructor() {
    super();
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 284,
      }
    }
  }

  getChildContext () {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  onNavResize = (navOpenState) => {
    this.setState({
      navOpenState,
    });
  }

  render() {
    return (
      <HashRouter>
        <Switch>
          <Route
            path='/'
            component={(props) => (
              <ProductApp
                onNavResize={this.onNavResize}
                {...props}
              >
                <Route exact path="/" component={HomePage} key="home" />
                <Route exact path="/jira" component={JiraPage} key="jira" />
                <Route exact path="/settings" component={SettingsPage} key="settings" />
                {
                  AppDAO.iterateModules(AppDAO.enabledAppsFilter, (app, moduleType, module) => {
                    const appPageUrl = AppLinkUtil.buildAppLink(app, module);
                    return (
                      <Route
                        exact
                        key={module.key}
                        path={appPageUrl}
                        component={GenericAppHostPage} />
                    );
                  })
                }
              </ProductApp>
            )}
          />
        </Switch>
      </HashRouter>
    );

  }
}

MainRouter.childContextTypes = {
  navOpenState: PropTypes.object,
}
