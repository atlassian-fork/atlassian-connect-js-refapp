import flags from './flags';
import settings from './settings';
import { combineReducers } from 'redux';
const rootReducer = combineReducers({
  flags,
  settings
});
export default rootReducer;
