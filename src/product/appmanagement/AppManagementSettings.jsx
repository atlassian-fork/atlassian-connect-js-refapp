import React, { PureComponent } from 'react';
import AddCircleIcon from '@atlaskit/icon/glyph/add-circle';
import AppDAO from "./AppDAO";
import AppDescriptorEditor from './AppDescriptorEditor';
import Button, { ButtonGroup } from '@atlaskit/button';
import EditIcon from '@atlaskit/icon/glyph/edit';
import RefreshIcon from '@atlaskit/icon/glyph/refresh';
import Toggle from '@atlaskit/toggle';
import TrashIcon from '@atlaskit/icon/glyph/trash';
import Modal from "@atlaskit/modal-dialog";

export default class AppManagementSettings extends PureComponent {

  state = {
    selectedAppToEditDescriptor: null,
    apps: AppDAO.getAllApps()
  };

  handleExtensionEnablementToggle = (event, app) => {
    const apps = AppDAO.getAllApps();
    for (var index = 0; index < apps.length; index++) {
      const thisApp = apps[index];
      if (thisApp.key === app.key) {
        thisApp.enabled = !thisApp.enabled;
      }
    }
    AppDAO.storeApps(apps);
    this.propogateSettingsChange(event);
  }

  handleAppDescriptorEdit = (event) => {
    const apps = AppDAO.getAllApps();
    this.setState({ apps: apps });
    this.propogateSettingsChange(event);
  }

  handleResetAppSettings = (event) => {
    const apps = AppDAO.resetToDefault();
    this.setState({ apps: apps });
    this.propogateSettingsChange(event);
  };

  handleAddApp = (event) => {
    const apps = AppDAO.getAllApps();
    const app = AppDAO.createApp();
    apps.push(app);
    AppDAO.storeApps(apps);
    this.setState({
      apps: apps
    });
    this.propogateSettingsChange(event);
  };

  propogateSettingsChange = (event) => {
    if (this.props.onAppSettingsChange) {
      // Note that it is necessary to pass the event through to keep redux happy.
      this.props.onAppSettingsChange(event);
    }
  };

  handleEditAppDescriptor = (event, app) => {
    event.preventDefault();
    this.setState({selectedAppToEditDescriptor: app});
  };

  handleDeleteApp = (event, app) => {
    const apps = AppDAO.deleteApp(app);
    this.setState({
      apps: apps
    });
    this.propogateSettingsChange(event);
  };

  saveAndCloseDescriptorEditor = () => {
    this.setState({selectedAppToEditDescriptor: null});
  };

  enumerateModules = (app) => {
    let moduleNames = '';
    let nextSeparator = '';
    AppDAO.iterateModulesInApp(app, (app, moduleType, module) => {
      moduleNames += nextSeparator + module.name.value;
      nextSeparator = ', ';
    });
    return (
      <span>
        {moduleNames}
      </span>
    );
  }

  render() {
    const actions = [
      { text: 'Save and close', onClick: this.saveAndCloseDescriptorEditor }
    ];
    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Name and description</th>
              <th>Modules</th>
              <th>Enabled</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.apps.map(app => (
                <tr
                  key={app.key}
                >
                  <td>
                    {app.name}
                    <br/>
                    <span
                      style={{color: '#777', fontSize: '0.8em'}}
                    >
                      {app.description}
                    </span>
                  </td>
                  <td>
                    {this.enumerateModules(app)}
                  </td>
                  <td>
                    <Toggle
                      label={'toggle-app-' + app.key}
                      isChecked={app.enabled}
                      isDefaultChecked={app.enabled}
                      onChange={(event) => this.handleExtensionEnablementToggle(event, app)}
                    />
                  </td>
                  <td>
                    <ButtonGroup>
                      <Button
                        appearance='default'
                        iconBefore={<EditIcon label='Edit app' />}
                        target='_blank'
                        onClick={(event) => this.handleEditAppDescriptor(event, app)}
                      >
                        Edit
                      </Button>
                      <Button
                        appearance='default'
                        iconBefore={<TrashIcon label='Delete app' />}
                        target='_blank'
                        onClick={(event) => this.handleDeleteApp(event, app)}
                      >
                        Delete
                      </Button>
                    </ButtonGroup>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

        <div
          style={{
            marginTop: '10px'
          }}
        >
          <ButtonGroup>
            <Button
              appearance='warning'
              iconBefore={<RefreshIcon label='' />}
              target='_blank'
              onClick={(event) => this.handleResetAppSettings(event)}
            >
              Reset app settings
            </Button>
            <Button
              appearance='primary'
              iconBefore={<AddCircleIcon primaryColor='#fff' secondaryColor='#0052CC' label='Set URL parameters' />}
              target='_blank'
              onClick={(event) => this.handleAddApp(event)}
            >
              Add an app
            </Button>
          </ButtonGroup>
        </div>

        {this.state.selectedAppToEditDescriptor && (
          <Modal
            actions={actions}
            onClose={this.saveAndCloseDescriptorEditor}
            heading={this.state.selectedAppToEditDescriptor.name + ' Descriptor'}
            width="x-large"
            shouldCloseOnOverlayClick={false}
          >
            <AppDescriptorEditor
              app={this.state.selectedAppToEditDescriptor}
              onAppSettingsChange={this.handleAppDescriptorEdit}
            />
          </Modal>
        )}
      </div>
    );
  }
}
