
const mockUserModule = {

  getUser: function (callback) {
    callback({fullName: 'Foo Bar', id: 'foo-bar-12345', key: 'foo-bar'});
  },

  getCurrentUser: function (callback) {
    callback({fullName: 'Alana', id: 'alana-12345', key: 'alana'});
  },

  getTimeZone: function (callback) {
    callback('Africa/Freetown');
  }

};

export default mockUserModule;
