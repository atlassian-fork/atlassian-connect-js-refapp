import React, { PureComponent } from 'react';

import LeftArrowIcon from '@atlaskit/icon/glyph/arrow-left';
import RightArrowIcon from '@atlaskit/icon/glyph/arrow-right';

export default class ArrowButton extends PureComponent {

  render() {
    const style = {
      display: 'inline-block',
      width: '32px',
      paddingLeft: '0px',
      paddingRight: '0px',
      borderRadius: '16px',
      lineHeight: '32px',
      fontSize: '24px',
      textAlign: 'center',
      color: '#777',
      fontWeight: 'bold',
      backgroundColor: '#eee',
      cursor: 'pointer'
    };
    if (this.props.left) {
      style.marginRight = '10px';
    } else {
      style.float = 'right';
    }
    return (
      <span
        style={style}
        onClick={(event) => this.props.onArrowClick(event)}
      >
        {
          this.props.left ? <LeftArrowIcon /> : <RightArrowIcon />
        }
      </span>
    );
  }

}
