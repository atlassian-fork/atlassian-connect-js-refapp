import React, { PureComponent } from 'react';
import Button from "@atlaskit/button";
import JiraIssueView from "./JiraIssueView";
import Modal from "@atlaskit/modal-dialog";

export default class JiraIssueDialog extends PureComponent {

  state = {
    isOpen: true
  };

  openIssueDialog = () => {
    this.setState({ isOpen: true });
  };

  closeIssueDialog = () => {
    this.setState({ isOpen: false });
  };

  render() {
    const actions = [
      { text: 'Close', onClick: this.closeIssueDialog }
    ];
    return (
      <div>
        <Button onClick={this.openIssueDialog}>Open Issue Dialog</Button>
        {this.state.isOpen && (
          <Modal
            actions={actions}
            onClose={this.closeIssueDialog}
            heading={this.props.issue.key + ': ' + this.props.issue.summary}
            width="x-large"
            shouldCloseOnOverlayClick={true}
          >
            <JiraIssueView
              webPanelModules={this.props.webPanelModules}
              issue={this.props.issue}
            />
          </Modal>
        )}
      </div>
    );
  }
}
