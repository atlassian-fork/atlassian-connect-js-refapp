import React, { PureComponent } from 'react';
import ContentWrapper from '../components/ContentWrapper';
import DropdownMenu, {DropdownItem, DropdownItemGroup} from '@atlaskit/dropdown-menu';
import PageTitle from '../components/PageTitle';
import acjsHighLevelImage from '../assets/acjs-high-level.png';
import ExternalLink from '../components/ExternalLink';

export class Doco {

  static NAMES_TO_URLS = {
    'Jira Platform': 'https://developer.atlassian.com/cloud/jira/platform',
    'Jira Service Desk': 'https://developer.atlassian.com/cloud/jira/service-desk',
    'Jira Software': 'https://developer.atlassian.com/cloud/jira/software',
    'Confluence Cloud': 'https://developer.atlassian.com/cloud/confluence/'
  };

}


export default class HomePage extends PureComponent {

  state = {
    selectedDocoSetName: 'Choose a product'
  }

  onDocoItemClicked = (event) => {
    console.log('Doco item:', event);
  };

  renderExternalLink = (url, text) => {
    const linkContent = text ? text : url;
    return (
      <ExternalLink
        href={url}
        label={linkContent}
      >
        {linkContent}
      </ExternalLink>
    );
  }

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Atlassian Connect JS Reference Application</PageTitle>

        <h2>Introduction</h2>

        <img src={acjsHighLevelImage} alt="ACJS overview" style={{float: 'right', maxWidth: '266px'}}/>

        <p>
          Atlassian Connect JS (ACJS) is an Atlassian client side framework allowing front end integration of
          third party apps (add-ons, applications, etc) with Atlassian products (Jira, Confluence, etc).
        </p>

        <p>
          This application has been built by the ACJS team to provide:
        </p>

        <ul>
          <li>
            Atlassian product teams with a <i>reference</i> implementation of how to incorporate ACJS into their product.
          </li>
          <li>
            App developers with a demonstration of the capabilities of the ACJS framework.
          </li>
        </ul>


        <h2>Terminology</h2>

        <ul>
          <li>
            <b>ACJS Refapp</b>: Short for 'Atlassian Connect JS Reference Application' which refers to this application.
          </li>
          <li>
            <b>Host</b>: An application hosting the ACJS framework such as Jira, Confluence and the ACJS Refapp.
          </li>
          <li>
            <b>App</b>: An app that anyone can create and list in the Atlassian Marketplace. The ACJS Refapp includes two apps:
            <ul>
              <li>
                Hello World: A very simple starter app.
              </li>
              <li>
                Kitchen Sink: An app with a lot of functionality for the purpose of demonstrating the capabilities of the ACJS framework.
              </li>
            </ul>
          </li>
        </ul>


        <h2>Documentation</h2>

        <p>
          Choose a product to find the best documentation for you:
        </p>

        <DropdownMenu
          trigger={this.state.selectedDocoSetName}
          defaultOpen={false}
          onClick={this.onDocoItemClicked}
          triggerType="button"
          shouldFitContainer={false}
        >
          <DropdownItemGroup id="jira-doco-set" title="Jira">
            <DropdownItem id="jira-platform-doco" onClick={() => this.setState({selectedDocoSetName: 'Jira Platform'})}>Platform</DropdownItem>
            <DropdownItem id="jira-service-desk-doco" onClick={() => this.setState({selectedDocoSetName: 'Jira Service Desk'})}>Service Desk</DropdownItem>
            <DropdownItem id="jira-software-doco" onClick={() => this.setState({selectedDocoSetName: 'Jira Software'})}>Software</DropdownItem>
          </DropdownItemGroup>
          <DropdownItemGroup id="confluence-doco-set" title="Confluence">
            <DropdownItem id="confluence-cloud-doco" onClick={() => this.setState({selectedDocoSetName: 'Confluence Cloud'})}>Confluence Cloud</DropdownItem>
          </DropdownItemGroup>
        </DropdownMenu>

        {
          this.state.selectedDocoSetName === 'Choose a product' ?
          null
          :
          <p>
            For {this.state.selectedDocoSetName}, visit {this.renderExternalLink(Doco.NAMES_TO_URLS[this.state.selectedDocoSetName])}.
          </p>
        }

        <h2>Contributing</h2>

        <p>
          ACJS is an open source project. Here are some useful resources if wish to contribute:
        </p>

        <ul>
          <li>
            <b>ACJS Refapp source code</b>: {this.renderExternalLink('https://bitbucket.org/atlassian/atlassian-connect-js-refapp')}
          </li>
          <li>
            <b>ACJS source code</b>: {this.renderExternalLink('https://bitbucket.org/atlassian/atlassian-connect-js')}
          </li>
          <li>
            <b>Logging an issue</b>: {this.renderExternalLink('https://ecosystem.atlassian.net/servicedesk/customer/portal/14')}
          </li>
          <li>
            <b>Viewing our issue backlog</b>: {this.renderExternalLink('https://ecosystem.atlassian.net/browse/ACJS')}
          </li>
          <li>
            <b>Developer Community</b>: {this.renderExternalLink('https://community.developer.atlassian.com/')}
          </li>
          <li>
            <b>AtlasKit</b>: {this.renderExternalLink('https://atlaskit.atlassian.com/')}
          </li>
          <li>
            <b>Atlassian Design Guidelines</b>: {this.renderExternalLink('https://atlassian.design/')}
          </li>
        </ul>

      </ContentWrapper>
    );
  }
}
