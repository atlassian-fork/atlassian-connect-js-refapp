export default class ProductDropdownProxy {

  static lastValue = 0;

  constructor(dropdownProviderCreationOptions, dropdownProvider) {
    this.state = {
      dropdownId: dropdownProviderCreationOptions.dropdownId,
      dropdownProvider: dropdownProvider,
      isVisible: false,
      dropdownGroups: dropdownProviderCreationOptions.dropdownGroups || [],
      x: 0,
      y: 0,
      width: 0,
      itemNotificationCallback: dropdownProviderCreationOptions.dropdownItemNotifier
    }

    this.state.dropdownGroups.forEach((dropdownGroup) => {
      if (!dropdownGroup.value) {
        ProductDropdownProxy.lastValue++;
        dropdownGroup.value = 'group-' + ProductDropdownProxy.lastValue;
      }
      dropdownGroup.items.forEach((dropdownItem) => {
        dropdownItem.value = dropdownItem.itemId;
        if (!dropdownItem.value) {
          ProductDropdownProxy.lastValue++;
          dropdownItem.value = 'item-' + ProductDropdownProxy.lastValue;
        }
      });
    });
  }

  showAt(x, y, width) {
    this.state.isVisible = true;
    this.state.x = x;
    this.state.y = y;
    this.state.width = width;
  }

  hide() {
    this.state.isVisible = false;
  }

  isVisible = () => {
    return this.state.isVisible;
  }

  _setInNestedList(list, item, attribute, value) {
    let counter = 0;
    list = list.map((listItem) => {
      if(listItem.items) {
        listItem.items = this._setInNestedList(listItem.items, item, attribute, value);
      } else if(typeof item === 'string' && listItem.value === item) {
        listItem[attribute] = value;
      } else if(typeof item === 'number' && counter === item) {
        list[item][attribute] = value;
      }
      counter++;
      return listItem;
    });
    return list;
  }

  itemEnabled(item, isEnabled) {
    this.state.dropdownGroups = this._setInNestedList(this.state.dropdownGroups, item, 'isDisabled', !isEnabled);
  }
}
