import * as React from 'react';
import {mount} from 'enzyme';
import { AtlasKitMessageView, AtlasKitMessageProvider } from '../AtlasKitMessageProvider';
import { MessageProviderValidator } from '@atlassian/connect-module-core';

/**
 * This test validates our implementation of the MessageProvider. The validation logic is owned
 * by connect-module-core.
 */
describe('MessageProviderValidationTesting', () => {

  it('Should implement the MessageProvider API properly.', () => {
    const messageProvider = new AtlasKitMessageProvider();
    mount(<AtlasKitMessageView messageProvider={messageProvider} />);
    MessageProviderValidator.validate(messageProvider);
  });

});