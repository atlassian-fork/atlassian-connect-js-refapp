import { FlagProvider } from '@atlassian/connect-module-core';
import React, { PureComponent } from 'react';
import FlagView from '../components/connect/FlagView';

export class AtlasKitFlagProvider implements FlagProvider {

  flagView = null;

  /**
   * @param flagCreationOptions - for type info, see @atlassian/connect-module-core/lib/modules/flag/FlagCreationOptions.
   */
  create = (flagProviderCreationOptions) => {
    return this.flagView.createFlag(flagProviderCreationOptions);
  };

  isFlagOpen(flagId) {
    return this.flagView.isFlagOpen(flagId);
  }

  setView = (flagView) => {
    this.flagView = flagView;
  }

}

export class AtlasKitFlagView extends PureComponent {

  state = {
    allFlagCreationOptions: [] // using an array keeps the allFlagCreationOptions ordered
  };

  componentDidMount() {
    this.props.flagProvider.setView(this);
  }

  componentDidUpdate() {
    this.props.flagProvider.setView(this);
  }

  /**
   * @param flagCreationOptions - for type info, see @atlassian/connect-module-core/lib/modules/flag/FlagCreationOptions.
   */
  createFlag = (flagProviderCreationOptions) => {
    const actions = this.buildAtlasKitFlagActionsFromProviderActions(flagProviderCreationOptions.actions);
    const thisFlagCreationOptions = {
      'id': flagProviderCreationOptions.id,
      'title': flagProviderCreationOptions.title,
      'description': flagProviderCreationOptions.body,
      'type': flagProviderCreationOptions.type,
      'close': flagProviderCreationOptions.close,
      'onClose': flagProviderCreationOptions.onClose,
      'actions': actions
    };
    this.setState({
      allFlagCreationOptions: this.state.allFlagCreationOptions.concat([thisFlagCreationOptions])
    });
    const flagHandle = {
      id: thisFlagCreationOptions.id,
      close: () => {
        this.closeFlag(thisFlagCreationOptions.id)
      }
    };
    return flagHandle;
  };

  isFlagOpen(flagId) {
    var foundFlags = this.state.allFlagCreationOptions.filter(function(flagCreationOptions) {
      return flagCreationOptions.id === flagId;
    });
    return foundFlags.length > 0;
  }

  buildAtlasKitFlagActionsFromProviderActions = (providerActions) => {
    let actions = [];
    if (providerActions) {
      for (let i = 0; i < providerActions.length; i++) {
        const actionOption = providerActions[i];
        const action = {
          key: actionOption.actionKey,
          content: actionOption.actionText,
          onClick: actionOption.executeAction
        };
        actions.push(action);
      }
    }
    return actions;
  };

  closeFlag = (flagId) => {
    this.setState(currentState => {
      currentState.allFlagCreationOptions.forEach(flagCreationOptions => {
        if (flagCreationOptions.id === flagId) {
          flagCreationOptions.onClose(flagId);
        }
      });
      const newFlags = currentState.allFlagCreationOptions.filter(flagCreationOptions => {
        return flagCreationOptions.id !== flagId;
      });
      return {
        allFlagCreationOptions: newFlags
      };
    });
  };

  render() {
    return (
      <FlagView
        allFlagCreationOptions={this.state.allFlagCreationOptions}
        actions={{
          dismissFlag: this.closeFlag
        }}
        onDismissed={this.closeFlag}
      />
    );
  }
}
