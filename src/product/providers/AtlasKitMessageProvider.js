import React, { PureComponent } from 'react';
import MessageView from '../components/connect/MessageView';

export class AtlasKitMessageProvider implements MessageProvider {

  messageView = null;

  generic = (title, body, messageCreationOptions) => {
    const message = this.messageView.createMessage('generic', title, body, messageCreationOptions);
    return message;
  };

  hint = (title, body, messageCreationOptions) => {
    const message = this.messageView.createMessage('hint', title, body, messageCreationOptions);
    return message;
  };

  info = (title, body, messageCreationOptions) => {
    const message = this.messageView.createMessage('info', title, body, messageCreationOptions);
    return message;
  };

  success = (title, body, messageCreationOptions) => {
    const message = this.messageView.createMessage('success', title, body, messageCreationOptions);
    return message;
  };

  warning = (title, body, messageCreationOptions) => {
    const message = this.messageView.createMessage('warning', title, body, messageCreationOptions);
    return message;
  };

  error = (title, body, messageCreationOptions) => {
    const message = this.messageView.createMessage('error', title, body, messageCreationOptions);
    return message;
  };

  clear = (messageId) => {
    this.messageView.closeMessage(messageId);
  };

  onClose = (messageId, callback) => {
    this.messageView.onClose(messageId, callback);
  };

  setView = (messageView) => {
    this.messageView = messageView;
  }

}

export class AtlasKitMessageView extends PureComponent {

  messageIdsToCloseHandlers = {};

  state = {
    messages: [] // using an array keeps the messages ordered
  };

  componentDidMount() {
    this.props.messageProvider.setView(this);
  }

  componentDidUpdate() {
    this.props.messageProvider.setView(this);
  }

  /**
   * @param messageCreationOptions - for type info, see @atlassian/connect-module-core/lib/modules/message/MessageCreationOptions.
   */
  createMessage = (type, title, body, messageCreationOptions) => {
    const message = {
      'id': messageCreationOptions.id,
      'key': messageCreationOptions.id,
      'type': type,
      'title': title,
      'body': body,
      'closeable': messageCreationOptions.closeable,
      'fadeout': messageCreationOptions.fadeout,
      'delay': messageCreationOptions.delay,
      'duration': messageCreationOptions.duration
    };
    this.setState({
      messages: this.state.messages.concat([message])
    });
    return message;
  };

  closeMessage = (messageId) => {
    this.state.messages.forEach((message) => {
      if (message.id === messageId) {
        const closeHandler = this.messageIdsToCloseHandlers[messageId];
        if (closeHandler) {
          try {
            closeHandler();
          } catch (exception) {
            console.error('Message close handler threw and exception: ', exception);
          }
          delete this.messageIdsToCloseHandlers[messageId];
        }
      }
    });
    var newMessages = this.state.messages.filter(function(message) {
      return message.id !== messageId;
    });
    this.setState({
      messages: newMessages
    });
  };

  onClose = (messageId, callback) => {
    this.messageIdsToCloseHandlers[messageId] = callback;
  };

  render() {
    return (
      <MessageView
        messages={this.state.messages}
        actions={{
          dismissMessage: this.closeMessage
        }}
      />
    );
  }
}
