export const loadSettings = () => {
  try {
    const serialisedSettings = localStorage.getItem('settings');
    if (serialisedSettings === null) {
      return undefined;
    }
    return JSON.parse(serialisedSettings);
  } catch (err) {
    return undefined;
  }
};

export const saveSettings = settings => {
  try {
    const serialisedSettings = JSON.stringify(settings);
    localStorage.setItem('settings', serialisedSettings);
  } catch (err) {
    // Ingored
  }
};