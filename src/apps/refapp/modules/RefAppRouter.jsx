import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import RefApp from './RefApp.jsx';
import JiraPage from '../pages/JiraPage.jsx';
import ConfluenceMacrosPage from '../pages/ConfluenceMacrosPage.jsx';
import HomePage from '../pages/HomePage.jsx';
import FlagPage from '../pages/FlagPage.jsx';
import DialogPage from '../pages/DialogPage.jsx';
import SizingPage from '../pages/SizingPage.jsx';
import MessagePage from '../pages/MessagePage.jsx';
import NestedAppPage from '../pages/NestedAppPage.jsx'
import InlineDialogPage from '../pages/InlineDialogPage.jsx';
import DropdownPage from '../pages/DropdownPage.jsx';
import ApiDocoPage from '../pages/ApiDocoPage.jsx';
import ScrollPage from '../pages/ScrollPage.jsx';
import RefappUtil from '../../../common/RefappUtil';
import { AtlasKitThemeProvider } from '@atlaskit/theme';

export default class AppRouter extends PureComponent {
  constructor() {
    super();
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 284,
      }
    }
  }

  getChildContext () {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  appWithPersistentNav = () => {
    const theme = RefappUtil.getUrlParameterByName('theme') || 'light';
    return (props) => (
      <AtlasKitThemeProvider mode={theme}>
        <RefApp
          onNavResize={this.onNavResize}
          theme={theme}
          {...props}
        >
          <Route path="/app/refapp-app-home" component={HomePage} />
          <Route path="/app/refapp-app-sizing" component={SizingPage} />
          <Route path="/app/refapp-app-message" component={MessagePage} />
          <Route path="/app/refapp-app-flag" component={FlagPage} />
          <Route path="/app/refapp-app-dialog" component={DialogPage} />
          <Route path="/app/refapp-app-inlinedialog" component={InlineDialogPage} />
          <Route path="/app/refapp-app-dropdown" component={DropdownPage} />
          <Route path="/app/refapp-app-scroll" component={ScrollPage} />
          <Route path="/app/refapp-app-nested-app" component={NestedAppPage} />
          <Route path="/app/refapp-app-api-docs" component={ApiDocoPage} />
          <Route path="/app/refapp-app-jira" component={JiraPage} />
          <Route path="/app/refapp-app-confluence-macros" component={ConfluenceMacrosPage} />
        </RefApp>
      </AtlasKitThemeProvider>
    );
  };

  onNavResize = (navOpenState) => {
    this.setState({
      navOpenState,
    });
  };

  render() {
    return (
      <Router>
        <Route component={this.appWithPersistentNav()} />
      </Router>
    );
  }
}

AppRouter.childContextTypes = {
  navOpenState: PropTypes.object,
};
