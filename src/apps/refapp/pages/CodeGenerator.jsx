import React, { Component } from 'react';

import ContentWrapper from '../components/ContentWrapper';

export default class CodeGeneratorPage extends Component {
  static API = {
    dialog: { name: 'dialog', global: false },
    request: { name: 'request', global: true },
    flag: { name: 'flag', global: false },
    message: { name: 'message', global: false },
    user: { name: 'user', global: false }
  };

  getCode = (api, options) => {
    const methodName = this.props.methodName ? this.props.methodName : 'create';
    const parameters = options ? this.prettifyJson(options) : '';

    if (api === 'dialog') {
      options.key = 'dialog-key';
    }

    if (api.global) {
      return ` AP.${api.name}(${parameters});`;
    } else {
      return ` AP.${api.name}.${methodName}(${parameters});`;
    }
  };

  prettifyJson = json => {
    const prettified = JSON.stringify(json, undefined, 2);
    return prettified;
  };

  render() {
    return (
      <ContentWrapper>
        <div>
          <h4>Copy the code and try it yourself!</h4>
          <hr />
          <br />
        </div>

        <div>
          <pre>{this.getCode(this.props.api, this.props.options)}</pre>
        </div>
      </ContentWrapper>
    );
  }
}
