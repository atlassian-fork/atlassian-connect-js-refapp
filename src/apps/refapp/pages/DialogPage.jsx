import Button from '@atlaskit/button';
import Toggle from '@atlaskit/toggle';
import React, { Component } from 'react';
import Tabs from '@atlaskit/tabs';
import { Label } from '@atlaskit/field-base'; //ref: AK-1621
import FieldText from '@atlaskit/field-text';
import InfoIcon from '@atlaskit/icon/glyph/info';
import SingleSelect from '@atlaskit/single-select';
import AkDynamicTable from '@atlaskit/dynamic-table';
import Page, { Grid, GridColumn } from '@atlaskit/page';

import PageTitle from '../components/PageTitle';
import RefappUtil from '../../../common/RefappUtil';
import ContentWrapper from '../components/ContentWrapper';
import CodeGenerator from './CodeGenerator';

/* global AP */

export default class DialogPage extends Component {
  state = {
    size: 'medium',
    width: '',
    height: '',
    chrome: true,
    header: 'header',
    submitText: '',
    cancelText: '',
    customData: { key: 'value' },
    closeOnEscape: true,
    buttons: [],
    hint: 'Dialogs can have hints',
    nextDialog: ''
  };

  createDialog = nested => {
    const cloneState = this.cloneState();
    AP.dialog.create({
      key: nested
        ? 'basic-dialog-module-' + this.state.nextDialog
        : 'basic-dialog-module',
      ...cloneState
    });
  };

  cloneState = () => {
    const cloneState = JSON.parse(JSON.stringify(this.state));
    delete cloneState.nextDialog;
    if (cloneState.size) {
      delete cloneState.width;
      delete cloneState.height;
    }

    return cloneState;
  };

  addButton = () => {
    let buttons = this.state.buttons;
    let identifier = Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, '')
      .substr(0, 5);
    buttons.push({
      key: identifier,
      identifier,
      text: identifier,
      disabled: false
    });
    this.setState({ buttons });
  };

  deleteButton = identifier => {
    let buttons = this.state.buttons.filter(
      button => button.identifier !== identifier
    );
    this.setState({ buttons });
  };

  updateButton = (identifier, obj) => {
    let buttons = this.state.buttons.map(
      button =>
        button.identifier === identifier ? Object.assign(button, obj) : button
    );
    this.setState({ buttons });
  };

  componentWillMount() {
    const nextDialog = RefappUtil.getUrlParameterByName('nextDialog');
    this.setState({ nextDialog });
  }

  componentDidMount() {
    [
      'dialog.submit',
      'dialog.cancel',
      'dialog.button.click',
      'dialog.close'
    ].forEach(eventName => {
      AP.events.on(eventName, data => {
        if (data && data.button) {
          const isSubmitOrCancel =
            data.button.identifier === 'submit' ||
            data.button.identifier === 'cancel';
          AP.flag.create({
            title: `Dialog button event`,
            body: `'${eventName}' event received in the page that launched the dialog. Event details: ${JSON.stringify(
              data,
              null,
              2
            )} `,
            type: 'success',
            close: isSubmitOrCancel ? 'auto' : 'manual'
          });
        }
      });
    });
  }

  render() {
    const sizes = [
      {
        items: [
          { content: 'explicit', value: '' },
          { content: 'small', value: 'small' },
          { content: 'medium', value: 'medium', defaultSelected: true },
          { content: 'large', value: 'large' },
          { content: 'xlarge', value: 'xlarge' },
          { content: 'x-large', value: 'x-large' },
          { content: 'fullscreen', value: 'fullscreen' }
        ]
      }
    ];

    const head = {
      cells: [
        {
          key: 'id',
          content: 'Id'
        },
        {
          key: 'text',
          content: 'Text'
        },
        {
          key: 'disabled',
          content: 'Disabled'
        },
        {
          key: 'actions',
          content: 'Actions'
        }
      ]
    };

    return (
      <ContentWrapper>
        <div>
          <PageTitle>Dialog</PageTitle>
          <Button
            appearance="subtle-link"
            iconBefore={<InfoIcon label="" />}
            target="_blank"
            href={
              'https://developer.atlassian.com/cloud/jira/platform/jsapi/classes/dialogoptions'
            }
          >
            Documentation
          </Button>
          <div style={{ float: 'right' }}>
            <Button
              appearance="primary"
              onClick={() => this.createDialog(this.props.nested)}
              isDisabled={this.props.nested && !this.state.nextDialog}
            >
              {this.props.nested ? (
                <p>Create another dialog</p>
              ) : (
                <p>Create dialog</p>
              )}
            </Button>
          </div>
        </div>
        <Tabs
          tabs={[
            {
              content: (
                <Page>
                  <Grid>
                    <GridColumn>
                      <FieldText
                        label="header"
                        onChange={e =>
                          this.setState({ header: e.target.value })
                        }
                        value={this.state.header}
                        placeholder="header"
                      />
                      <FieldText
                        label="submit text"
                        onChange={e =>
                          this.setState({ submitText: e.target.value })
                        }
                        value={this.state.submitText}
                      />
                      <FieldText
                        label="cancel text"
                        onChange={e =>
                          this.setState({ cancelText: e.target.value })
                        }
                        value={this.state.cancelText}
                      />
                      <FieldText
                        label="hint text"
                        onChange={e => this.setState({ hint: e.target.value })}
                        value={this.state.hint}
                      />
                      <div>
                        <Label label="chrome" />
                        <Toggle
                          label="chrome"
                          isChecked={this.state.chrome}
                          isDefaultChecked={this.state.chrome}
                          onChange={e =>
                            this.setState({ chrome: e.target.checked })
                          }
                        />
                      </div>
                      <br />
                      <Button onClick={() => AP.dialog.disableCloseOnSubmit()}>
                        disableCloseOnSubmit()
                      </Button>
                    </GridColumn>
                    <GridColumn>
                      <SingleSelect
                        label="size:"
                        items={sizes}
                        onSelected={e => this.setState({ size: e.item.value })}
                        defaultSelected={
                          sizes[0].items.filter(
                            size => size.value === this.state.size
                          )[0]
                        }
                      />
                      {this.state.size ? null : (
                        <div>
                          <FieldText
                            label="width"
                            onChange={e =>
                              this.setState({ width: e.target.value })
                            }
                            value={this.state.width}
                          />
                          <FieldText
                            label="height"
                            onChange={e =>
                              this.setState({ height: e.target.value })
                            }
                            value={this.state.height}
                          />
                        </div>
                      )}
                      <FieldText
                        label="custom data"
                        onChange={e =>
                          this.setState({
                            customData: JSON.parse(e.target.value)
                          })
                        }
                        value={JSON.stringify(this.state.customData)}
                      />
                      <div>
                        <Label label="closeOnEscape" />
                        <Toggle
                          label="closeOnEscape"
                          isChecked={this.state.closeOnEscape}
                          isDefaultChecked={this.state.closeOnEscape}
                          onChange={e =>
                            this.setState({ closeOnEscape: e.target.checked })
                          }
                        />
                      </div>
                    </GridColumn>
                  </Grid>
                </Page>
              ),
              defaultSelected: true,
              label: 'General Options'
            },
            {
              content: (
                <Page>
                  <br />
                  <Button onClick={this.addButton}>+ Add Button</Button>
                  <br />
                  {this.state.buttons.length > 0 ? (
                    <AkDynamicTable
                      head={head}
                      rows={this.state.buttons.map(button => ({
                        cells: [
                          {
                            content: <p>{button.identifier}</p>
                          },
                          {
                            content: (
                              <FieldText
                                label={''}
                                isLabelHidden={true}
                                value={button.text}
                                onChange={e =>
                                  this.updateButton(button.id, {
                                    text: e.target.value
                                  })
                                }
                              />
                            )
                          },
                          {
                            content: (
                              <Toggle
                                label="disabled"
                                isChecked={button.disabled}
                                isDefaultChecked={button.disabled}
                                onChange={e =>
                                  this.updateButton(button.id, {
                                    disabled: e.target.checked
                                  })
                                }
                              />
                            )
                          },
                          {
                            content: (
                              <Button
                                appearance="link"
                                onClick={() => this.deleteButton(button.id)}
                              >
                                Delete
                              </Button>
                            )
                          }
                        ]
                      }))}
                      rowsPerPage={8}
                      defaultPage={1}
                    />
                  ) : null}
                </Page>
              ),
              label: 'Custom Buttons'
            },
            {
              content: (
                <CodeGenerator
                  api={CodeGenerator.API.dialog}
                  options={this.cloneState()}
                />
              ),
              label: 'Get Code'
            }
          ]}
        />
      </ContentWrapper>
    );
  }
}
