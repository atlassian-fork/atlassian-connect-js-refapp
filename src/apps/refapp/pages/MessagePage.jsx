import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import ContentWrapper from '../components/ContentWrapper';
import FieldRadioGroup from '@atlaskit/field-radio-group';
import PageTitle from '../components/PageTitle';

/* global AP */

class MessageDetails extends PureComponent {

  closeMessage = () => {
    console.log('In closeMessage');
    this.props.closeMessage(this.props.message);
  };

  render() {
    const messageContainerStyle = {
      'margin': '0.5em 0 0.5em 0',
      'padding': '0.5em',
      'border': '1px solid #ccc',
      'borderRadius': '5px'
    };
    const messageIdStyle = {
      'marginRight': '20px'
    };
    return (
      <div style={messageContainerStyle} onClick={(event) => this.closeMessage()}>
        <span style={messageIdStyle}>
          {this.props.message.messageKey}
        </span>
        <Button
          appearance="default"
          onClick={(event) => this.closeMessage()}
        >Close</Button>
      </div>
    );
  }

}

class NoMessagesIndicator extends PureComponent {

  render() {
    const messageStyle = {
      'margin': '0.5em 0 0.5em 0'
    };
    return (
      <div style={messageStyle}>
        There shouldn't be any messages open. Are there?
      </div>
    );
  }

}

export default class MessagePage extends PureComponent {

  static lastMessageKey = 0;

  messageTypesToCreators = {
    'generic': AP.messages.generic,
    'hint': AP.messages.hint,
    'info': AP.messages.info,
    'success': AP.messages.success,
    'warning': AP.messages.warning,
    'error': AP.messages.error
  };

  state = {
    selectedMessageType: 'info',
    messages: [] // current messages in chronological order by creation time
  };

  constructor(props) {
    super(props);
    AP.events.on('message.action', (data) => {
      console.log('Received an action event on message id', data.messageIdentifier, ', message action is ', data.actionIdentifier, '.');
    });
    AP.events.on('message.close', (data) => {
      console.log('Received event that message id', data.messageIdentifier, 'has been closed.');
      this.closeMessageWithId(data.messageIdentifier);
    });
  }

  createMessage = () => {
    MessagePage.lastMessageKey++;
    const messageKey = 'Message ' + MessagePage.lastMessageKey;
    const messageType = this.state.selectedMessageType;
    const create = this.messageTypesToCreators[messageType];
    const messagetTypeText = `${/^[aeiou]/.test(messageType.toLowerCase()) ? 'an' : 'a'} ${messageType}`;
    const title = `${messageKey}: Test message`;
    const body = 'Successfully created ' + messagetTypeText +' message.';
    const options = {};
    const message = create(title, body, options);
    message.messageKey = messageKey;
    AP.messages.onClose(message, () => {
      this.removeMessageById(message._id);
    });
    const newMessages = this.state.messages.concat([message]);
    this.setState({
      messages: newMessages
    });
  };

  closeMessage = (message) => {
    this.closeMessageWithId(message._id);
  };

  closeMessageWithId = (messageId) => {
    this.state.messages.forEach(message => {
      if (message._id === messageId) {
        AP.messages.clear(message);
      }
    });
    this.removeMessageById(messageId);
  };

  removeMessageById = (messageId) => {
    const filteredMessages = this.state.messages.filter(message => message._id !== messageId);
    this.setState({
      messages: filteredMessages
    });
  };

  render() {
    const items = [
      { name: 'messageType', value: 'generic', label: 'generic' },
      { name: 'messageType', value: 'hint', label: 'hint' },
      { name: 'messageType', value: 'info', label: 'info', defaultSelected: true },
      { name: 'messageType', value: 'success', label: 'success' },
      { name: 'messageType', value: 'warning', label: 'warning' },
      { name: 'messageType', value: 'error', label: 'error' }
    ];
    const allMessageDetails =
      this.state.messages.length ?
        this.state.messages.map((message) => {
          return (
            <MessageDetails
              key={message._id}
              message={message}
              closeMessage={this.closeMessage}
            />
          )
        }) :
        <NoMessagesIndicator/>;
    return (
      <ContentWrapper>
        <PageTitle>Message</PageTitle>

        <h3>Functionality</h3>

        <FieldRadioGroup
          items={items}
          label="Select a message type:"
          onRadioChange={(e) => {
            this.setState({
              selectedMessageType: e.target.value
            });
          }}
        />

        <Button
            appearance="primary"
            onClick={(event) => this.createMessage()}
        >Create message</Button>

        <h3>Current messages</h3>
        {allMessageDetails}

      </ContentWrapper>
    );
  }
}
