import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import InstallAppPanel from '../shared/InstallAppPanel';
import SharedAppUtil from '../shared/SharedAppUtil';
import VidShareScreenIcon from '@atlaskit/icon/glyph/vid-share-screen';
import Tabs from '@atlaskit/tabs';
import CodeGenerator from '../refapp/pages/CodeGenerator';

/* global AP */

export default class UserApp extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      lastUserResponse: undefined,
      lastCurrentUserResponse: undefined,
      lastTimeZoneResponse: undefined
    };
  }

  getUser = () => {
    AP.user.getUser((response) => {
      this.setState({
        lastUserResponse: response
      });
    });
  };

  getCurrentUser = () => {
    AP.user.getCurrentUser((response) => {
      this.setState({
        lastCurrentUserResponse: response
      });
    });
  };

  getTimeZone = () => {
    AP.user.getTimeZone((response) => {
      this.setState({
        lastTimeZoneResponse: response
      });
    });
  };

  renderResponse = (response) => {
    const prettifiedResponse = JSON.stringify(response, undefined, 2);
    return (
      <pre>
        {prettifiedResponse}
      </pre>
    );
  };

  renderGetUserTabs = () => {
    const style = {
      marginTop: 20
    };
    return (
      <div style={style}>

        <Tabs tabs={[
          {
            content: (
              <div style={style}>

                <h3>AP.user.getUser()</h3>
                <div style={style}>
                  <Button
                    appearance='primary'
                    iconAfter={<VidShareScreenIcon label='' />}
                    target='_blank'
                    onClick={this.getUser}
                  >
                    AP.user.getUser()
                  </Button>

                  <div style={style}>
                    {this.state.lastUserResponse && this.renderResponse(this.state.lastUserResponse)}
                  </div>
                </div>

              </div>
            ),
            label: "getUser and getTimeZone",
            defaultSelected: true
          },

          {
            content: (
              <div style={style}>

                <h3>AP.user.getCurrentUser()</h3>
                <div style={style}>
                  <Button
                    appearance='primary'
                    iconAfter={<VidShareScreenIcon label='' />}
                    target='_blank'
                    onClick={this.getCurrentUser}
                  >
                    AP.user.getCurrentUser()
                  </Button>

                  <div style={style}>
                    {this.state.lastCurrentUserResponse && this.renderResponse(this.state.lastCurrentUserResponse)}
                  </div>
                </div>

              </div>
            ),
            label: "getCurrentUser and getTimeZone",
            defaultSelected: true
          },

          {
            content: (
              <CodeGenerator
                api={CodeGenerator.API.user}
                methodName="getUser"
                options=""
              />
            ),
            label: "Get Code"
          }
        ]} />
      </div>
    );
  };

  renderGetTimeZoneTabs = () => {
    const style = {
      marginTop: '20px'
    };
    return (
      <div style={style}>

        <Tabs tabs={[
          {
            content: (
              <div style={style}>

                <h3>AP.user.getTimeZone()</h3>
                <div style={style}>
                  <Button
                    appearance='primary'
                    iconAfter={<VidShareScreenIcon label='' />}
                    target='_blank'
                    onClick={this.getTimeZone}
                  >
                    AP.user.getTimeZone()
                  </Button>

                  <div style={style}>
                    {this.state.lastTimeZoneResponse && this.renderResponse(this.state.lastTimeZoneResponse)}
                  </div>
                </div>

              </div>
            ),
            label: "Request Options",
            defaultSelected: true
          },

          {
            content: (
              <CodeGenerator
                api={CodeGenerator.API.user}
                methodName="getTimeZone"
                options=""
              />
            ),
            label: "Get Code"
          }
        ]} />
      </div>
    );
  };

  renderApAvailable = () => {
    const style = {
      marginTop: '20px'
    };
    return (
      <div style={style}>
        {this.renderInstallAppPanel()}
        {this.renderGetUserTabs()}
        {this.renderGetTimeZoneTabs()}
      </div>
    );
  };

  renderApUnavailable = () => {
    return (
      <div>
        <p>
        It looks like the user module is unavailable. :(
        </p>
        {this.renderInstallAppPanel()}
      </div>
    );
  };

  renderInstallAppPanel = () => {
    const appDescriptorUrl = SharedAppUtil.buildAppDescriptorUrl('user-app-connect-descriptor.json');
    return (
      <InstallAppPanel
        appName="User App"
        appDescriptorUrls={[appDescriptorUrl]}
      />
    );
  };

  render() {
    if (AP && AP.user) {
      return this.renderApAvailable();
    } else {
      return this.renderApUnavailable();
    }
  }

}
