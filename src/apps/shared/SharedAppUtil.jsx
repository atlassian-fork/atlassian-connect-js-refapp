
export default class SharedAppUtil {

  static iterateAppOptions(callback) {
    const dataOptions = SharedAppUtil.getDataOptions();
    if (dataOptions) {
      const optionItems = dataOptions.split(';');
      optionItems.forEach((optionItem) => {
        const keyValuePair = optionItem.split(':');
        if (keyValuePair.length === 2) {
          const key = keyValuePair[0];
          const value = keyValuePair[1];
          callback(key, value);
        } else {
          console.warn('Unexpected option: ', keyValuePair);
        }
      });
      console.log('dataOptions: ', dataOptions);
    }
  }

  static getDataOptions = () => {
    const head = document.getElementsByTagName('head')[0];
    const scripts = head.getElementsByTagName('script');
    for (var i = 0; i < scripts.length; i++) {
      const script = scripts[i];
      const dataOptions = script.getAttribute("data-options");
      if (dataOptions) {
        return dataOptions;
      }
    }
    return null;
  };

  static buildAppDescriptorUrl = (descriptorFilename) => {
    const protocol = window.location.protocol;
    const port = window.location.port;
    const suppressPort = !port || (port === 80 && protocol === 'http:') || (port === 443 && protocol === 'https:');
    const portSuffix = suppressPort ? '' : ':' + port;
    const appDescriptorUrl = protocol + '//' + window.location.hostname + portSuffix + '/app-descriptors/' + descriptorFilename;
    return appDescriptorUrl;
  };

}
