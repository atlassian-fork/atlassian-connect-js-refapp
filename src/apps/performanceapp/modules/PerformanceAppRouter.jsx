import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PerformanceApp from './PerformanceApp.jsx';
import HomePage from '../pages/HomePage.jsx';
import CacheableDialogPage from '../pages/CacheableDialogPage.jsx';
import RefappUtil from '../../../common/RefappUtil';
import { AtlasKitThemeProvider } from '@atlaskit/theme';

export default class AppRouter extends PureComponent {
  constructor() {
    super();
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 284,
      }
    }
  }

  getChildContext () {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  appWithPersistentNav = () => {
    const theme = RefappUtil.getUrlParameterByName('theme') || 'light';
    return (props) => (
      <AtlasKitThemeProvider mode={theme}>
        <PerformanceApp
          onNavResize={this.onNavResize}
          theme={theme}
          {...props}
        >
          <Route path="/app/perfapp-app-home" component={HomePage} />
          <Route path="/app/perfapp-app-cacheable-dialog" component={CacheableDialogPage} />
        </PerformanceApp>
      </AtlasKitThemeProvider>
    );
  };

  onNavResize = (navOpenState) => {
    this.setState({
      navOpenState,
    });
  };

  render() {
    return (
      <Router>
        <Route component={this.appWithPersistentNav()} />
      </Router>
    );
  }
}

AppRouter.childContextTypes = {
  navOpenState: PropTypes.object,
};
