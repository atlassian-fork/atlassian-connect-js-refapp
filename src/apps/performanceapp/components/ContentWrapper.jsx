import styled from 'styled-components';
import { gridSizeInt } from '../constants';

export default styled.div`
  margin: ${gridSizeInt * 4}px ${gridSizeInt * 8}px;
  padding-bottom: ${gridSizeInt * 3}px;
`;
