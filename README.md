# ACJS Refapp

This is a reference app for [ACJS](https://bitbucket.org/atlassian/atlassian-connect-js).

## Atlassians

See [internal developer info](https://extranet.atlassian.com/display/ECO/Developer+Info).

## Install Dependencies

```bash
yarn install
```

See also `scripts/postInstall.sh`.

If you don't have [Yarn](https://yarnpkg.com), install with:

```bash
npm install -g yarn
```

## Usage

```bash
npm run start
```

## Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes.
* Follow the existing style.
* Separate unrelated changes into multiple pull requests.

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## Pull Requests

We take the 'trust' approach when it comes to pull requests. This means that reviewers should
feel comfortable approving a PR with outstanding items due to trust in knowing that the PR
owner will address the actions as necessary. Use actions for PR items that are particularly
important since these require the PR owner to explicitly acknowledge them before merging.

## Repository Layout

The top level structure of the repo is:

* **docs** - _Internal developer documentation for [devhub](https://devhub.atlassian.io/atlassian-connect-js/)_
* **node_modules** - _Standard Node.js modules_
* **test** - _See End-to-End Testing section below_
* **src/apps** - _Root location of all apps (formerly known as add-ons)_
* **src/apps/refapp** - _Reference app or 'kitchen sink' of apps_
* **src/product** - _Product implementation serving as a reference integration of ACJS_

## Routing

All apps are served with a path starting '/app/'. The next part of an app path corresponds to the name of the app.

Other routing falls through to the host application (product).

## Dev Loop with ACJS

To develop [ACJS](https://bitbucket.org/atlassian/atlassian-connect-js) with this reference app,
you need to link your local atlassian-connect-js and connect-module-core repos as follows:

```bash
cd atlassian-connect-js
npm link
```

Then when you start this reference app, it will use your local versions of ACJS and connect-module-core:

```bash
cd atlassian-connect-js-refapp
npm link atlassian-connect-js
npm run start
```

Every time you `yarn install` (see above),
you need to `npm link atlassian-connect-js` again.

## Running Locally Like Production

From time to time, you may need to run locally from the built product. This is how you do it using
python, but any other static web server should also suffice:

```bash
npm run build
cd build
python -m SimpleHTTPServer 3000
```

## Unit Testing

CI is configured to run the 'bamboo' script. You can do this from the command line as follows:

```bash
npm run bamboo
```

However, to avoid the `yarn install` part (see above), just run:

```bash
npm test
```

## End-to-End Testing

There is a framework and some basic
[WebdriverIO](https://webdriver.io)
end-to-end tests with page objects in the test directory.

To run all tests locally in Chrome:

```bash
npm run build
npm run e2e_tests
```

To run a single spec locally:

```bash
npm run start
node_modules/webdriverio/bin/wdio test/wdio.conf.js --spec test/specs/<spec>.spec.js
```

These tests also run against the relevant
[supported browsers](https://confluence.atlassian.com/cloud/supported-browsers-744721663.html)
in BitBucket Pipelines.

## Static Deployments

To share a branch deployment in a pull request, create a static production build:

```bash
npm run build
```

Then deploy using [surge.sh](https://surge.sh):

```bash
surge -p ./build
```

## Generating Documentation

Run the following script to generate documentation of the components.

```bash
npm run build-react-docs
```

## Installing the Add-on in a Real Product

The add-on part of the ACJS Refapp can be installed into real products.

The following provides guidelines on how to do this for the production instance running at
[https://acjsrefapp.atl-paas.net](https://acjsrefapp.atl-paas.net),
but the same logic applies when running it elsewhere such as locally with an
[ngrok](https://ngrok.com) URL except you will need to modify
the base URL in public/add-on-connect-descriptor.json so that it doesn't point to the production instance.

### Installing in Confluence

Install the add-on in Confluence using the following URL:

https://acjsrefapp.atl-paas.net/confluence-app-connect-descriptor.json

In Confluence, the add-on will provide an 'ACJS Refapp' link in the main left menu.

### Installing in Jira

Install the add-on in Jira using the following URL:

https://acjsrefapp.atl-paas.net/jira-app-connect-descriptor.json

In Jira, the add-on will provide an 'ACJS Refapp' link in the main left menu.

## License

Copyright (c) 2017 Atlassian and others. Apache 2.0 licensed, see LICENSE.txt file.

### Big Thanks

Cross-browser Testing Platform and Open Source <3 are provided by [Sauce Labs](https://saucelabs.com).
