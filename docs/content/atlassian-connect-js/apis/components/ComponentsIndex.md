
---
title: Atlassian Connect JS Components
platform: acjs
product: acjs
category: reference
subcategory: components
date: '2017-03-31'
---

[//]: # (This page is generated so don't hand craft changes. See the 'Generating React Component Docs' section)
[//]: # (of the README.md for instructions about re-generating the docs.)

# Atlassian Connect JS Component

`ConnectApp` (component)
==========================

ConnectApp represents an add-on within a view.

Props
-----

### `appProvider`

An instance of AppProvider which provides capabilities to communicate between the
product and an add-on.

type: `object`


### `connectHost`

In order to decouple this component from atlassian-connect-js, the product is responsible for
passing in the instance of connectHost.

type: `object`


### `identifier`

TODO: this might change.

type: `string`



`ConnectProvider` (component)
=============================

This component is responsible for initialising the Connect capabilities of the product.

Props
-----

### `appProvider`

An instance of AppProvider which provides capabilities to communicate between the
product and an add-on.

type: `object`


### `connectHost`

In order to decouple this component from atlassian-connect-js, the product is responsible for
passing in the instance of connectHost.

type: `object`


### `dialogProvider`

An instance of DialogProvider which provides add-ons with dialog related capabilities.

type: `object`


### `flagActions`

An instance of FlagProvider which provides add-ons with flag related capabilities.

type: `object`



`BlueInfoIcon` (component)
==========================

This component renders a blue info icon.

This component has no props.

`GreenSuccessIcon` (component)
==============================

This component renders a green success icon.

This component has no props.

`RedErrorIcon` (component)
==========================

This component renders a red error icon.

This component has no props.

`YellowWarningIcon` (component)
===============================

This component renders a yellow warning icon.

This component has no props.

