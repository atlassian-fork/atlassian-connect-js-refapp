---
title: Terminology
platform: acjs
product: acjs
category: devguide
subcategory: intro
date: '2017-03-21'
---
# Terminology

## ACJS

An acronym for Atlassian Connect JS.

## Host

The product or application that ACJS is being served from.
